import React from 'react'

export default class extends React.Component {
    render() {
        const { title } = this.props
        return ( <header className="mdc-toolbar mdc-toolbar--fixed">
            <div className="mdc-toolbar__row">
                <section className="mdc-toolbar__section mdc-toolbar__section--align-start">
                    {this.props.menu}
                    <span className="mdc-toolbar__title">{title}</span>
                </section>
                <section className="mdc-toolbar__section mdc-toolbar__section--align-end">
                    {this.props.children}
                </section>
            </div>
        </header>
        )
    }
}
