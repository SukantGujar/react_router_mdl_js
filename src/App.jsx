import React from 'react'
import {Link} from 'react-router'
import './style.css'

export default props => <div id='site' className="mdc-typography">
    <Link to="v1"> View #1 </Link>
    <Link to="v2"> View #2 </Link>
    {props.children}
</div>
