import React from 'react'
import MdlLayout from './MdlLayout'
import {Link} from 'react-router'

export default () => <div>
    <MdlLayout title="View #2" menu={
        <Link to="v1">
            <button className="mdc-button mdc-ripple-upgraded">
                <i style={{color: "#FFF"}} className="material-icons">arrow_back</i>
            </button>
        </Link>}>
        <Link to="v1">
            <button className="mdc-button mdc-button--dense">View #1</button>
        </Link>
    </MdlLayout>
    <main className="mdc-toolbar-fixed-adjust">
        I`m view #2
    </main>
</div>