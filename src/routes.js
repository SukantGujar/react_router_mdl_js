import App from './App'
import View1 from './View1'
import View2 from './View2'

export default {
  path: '/',
  component: App,
  childRoutes: [
    { path: 'v1', component: View1 },
    { path: 'v2', component: View2 }
  ]
}
